﻿/*
Minesweeper
Author: Mo Harrim
2016
*/
(function () {
    // Building block class
    var Cell = function Cell(i, j, isMine) {
        // Properties of the Cell
        this.x = i;
        this.y = j;
        this.index = j + "-" + i;
        this.unrevealedNeighbors = 0;
        this.neighbors = [];
        this.isMine = isMine;
        this.revealed = false;
    };
    Cell.prototype.setValue = function (value) {
        // get cell (td) with the matching index
        var el = document.getElementById("minesweeper").querySelector("td[data-index='" + this.index + "']");
        el.innerHTML = value;
        if (value == "X")
            el.className = "mine";
        return this;
    };
    // Add adjacent cell (neighbor) 
    Cell.prototype.addNeighbor = function (neighbor) {
        this.neighbors.push(neighbor);
        this.unrevealedNeighbors += (neighbor.isMine ? 1 : 0);
        return this;
    };
    // get adjacent cells (neighbors)
    Cell.prototype.getNeighbors = function (neighbor) {
        return this.neighbors;
    };
    // Is clicked/revealed
    Cell.prototype.isRevealed = function () {
        return this.revealed || this.isMine;
    };
    // reveal cell value
    Cell.prototype.reveal = function (context) {
        // get cell (td) with the matching index
        var el = document.getElementById("minesweeper").querySelector("td[data-index='" + this.index + "']");
        if (this.isMine) {
            return;
        }
        // set cell to revealed
        this.revealed = true;
        // add css class active on td
        el.classList.add("active");
        // traverse thru neighbors
        // if zero then reveal neighbors
        this.setValue(this.unrevealedNeighbors);
        if (this.unrevealedNeighbors === 0) {
            this.getNeighbors().forEach(function (neighbor, i) {
                if (!neighbor.isRevealed()) {
                    neighbor.reveal(context);
                }
            });
        };
    };
    // The Table containing the cells
    var Table = {
        // array / collection of all cells
        arrCells: [],
        // list of mines to keep track of
        mines: {},
        reset: function (rows, cols, numMines) {
            var x, y;
            var tr;
            var that = this;
            this.cols = cols;
            this.rows = rows;
            // reset the arrCells
            this.arrCells = [];
            // reset mines list
            this.mines = {}; 
            // generate the and set the mines on the table
            this.randomizeMines(numMines);

            // create the array of cells
            for (y = 0; y < this.rows; y++) {
                tr = [];
                this.arrCells.push(tr);
                for (x = 0; x < this.cols; x++) {
                    var isMine = this.mines[y] && this.mines[y][x];
                    tr.push(new Cell(x, y, isMine));
                }
            }
            // assign neightbor reference for each cell
            this.loop(function (cell) {
                var neighbors = that.iterateNeighbors(cell);
                neighbors.forEach(function (value) {
                    cell.addNeighbor(value);
                });
            });
            return this;
        },
        // callback for cells
        // iterate thru cells
        loop: function (fn) {
            var x, y;
            for (y = 0; y < this.rows; y++) {
                for (x = 0; x < this.cols; x++) {
                    fn(this.getCell(x, y));
                }
            }
        },
        // create entire table with trs and tds
        create: function () {
            var i, j, tr, td, tbody;
            tbody = document.createElement("tbody");
            for (j = 0; j < this.rows; j++) {
                tr = document.createElement("tr");
                for (i = 0; i < this.cols; i++) {
                    //  assign index for each cell as data-index
                    td = document.createElement("td");
                    td.setAttribute("data-index", j + '-' + i);
                    td.innerHTML = "&nbsp;";
                    tr.appendChild(td);
                }
                tbody.appendChild(tr);
            }
            Minesweeper.over = false;
            document.getElementById("minesweeper").appendChild(tbody);
            return this;
        },
        // validate function
        // return false if unrevealed cells > number of mines
        // return true if unrevealed cells = mines
        validate: function () {
            var numUnrevealed = 0;
            this.loop(function (cell) {
                numUnrevealed += cell.isRevealed() ? 0 : 1;
            });
            return !numUnrevealed;
        },
        // reveal the cell value
        // if mine then game over
        revealCell: function (cell, passive) {
            cell.reveal(this);
            if (cell.isMine) {
                this.revealMines();
                Minesweeper.complete();
            }
        },
        getCell: function (x, y) {
            return this.arrCells[y][x];
        },
        // add mines randomly to the Board
        randomizeMines: function (numMines) {
            var i;
            for (i = 0; i < numMines; i++) {
                this.addMine();
            }
        },
        addMine: function () {
            var x = Math.floor(Math.random() * this.cols);
            var y = Math.floor(Math.random() * this.rows);
            // do not allow duplicates thus recursive call till it finds a mine-free cell
            if (this.mines[y] && this.mines[y][x]) 
                return this.addMine();
            // add to mines list
            this.mines[y] = this.mines[y] || {};
            this.mines[y][x] = true;
        },
        // show the location of mines
        // used for cheat and game complete or when clicking on a mine
        revealMines: function () {
            this.loop(function (cell) {
                if (cell.isMine) {
                    cell.setValue("X");
                }
            });
        },
        // retrieve specific cell by data-index
        getCellByIndex: function (e) {
            var cords = e.target.getAttribute('data-index').split('-');
            var y = cords[0];
            var x = cords[1];
            return this.getCell(x, y);
        },
        // iterate and return adjacent cells (neighbors)
        iterateNeighbors: function (cell) {
            var i, j;
            var data = [];
            var x = cell.x;
            var y = cell.y;
            for (i = y - 1; i <= y + 1; i++) {
                if (i < 0 || i >= this.rows) continue;
                for (j = x - 1; j <= x + 1; j++) {
                    if (j < 0 || j >= this.cols) continue;
                    if (x === j && y === i) continue;
                    data.push(this.getCell(j, i));
                }
            }
            return data;
        }
    };

    // Game & Event Controller
    var Minesweeper = {
        over: false,
        // on complete of the game whether by clicking on mine or hitting validate
        complete: function (win) {
            document.getElementById("minesweeper-status").innerHTML = win ? "Congratulations You Won!" : "Game Over...You Lost!";
            // success
            if (win) {
                document.getElementById("minesweeper-status").classList.add("alert-success");
            }
                // fail
            else {
                Table.revealMines();
                document.getElementById("minesweeper-status").classList.add("alert-error");
            }
            // append corresponding css classes
            document.getElementById("minesweeper").classList.remove("active");
            document.getElementById("minesweeper").classList.add("fail");
            this.over = true;
        },
        // Initialize the Minesweeper game given user-input options
        init: function (options) {
            // draw controls
            // status alert
            var frm = document.getElementById(options.formId);
            var statusdiv = document.createElement("div");
            statusdiv.setAttribute("id", "minesweeper-status");
            statusdiv.className = "minesweeper-alert";
            // minesweeper board table
            var msTable = document.createElement("table");
            msTable.setAttribute("id", "minesweeper");
            // Event delegation of the cell bubbled up to table click delegate
            msTable.onclick = function (e) {
                // e.target is the clicked element
                // making sure it's a cell (td) that was clicked
                if (e.target && e.target.nodeName.toUpperCase() == "TD") {
                    if (!Minesweeper.over) {
                        Table.revealCell(Table.getCellByIndex(e));
                    }
                }
            };
            // new game button
            var newgamebtn = document.createElement("input");
            newgamebtn.setAttribute("type", "button");
            newgamebtn.setAttribute("id", "minesweeper-new-game");
            newgamebtn.setAttribute("value", "New Game");
            newgamebtn.className = "minesweeper-bttn-main";
            newgamebtn.onclick = function (e) {
                e.preventDefault();
                frm.submit();
            };
            // validate button
            var validatebtn = document.createElement("input");
            validatebtn.setAttribute("type", "button");
            validatebtn.setAttribute("id", "minesweeper-validate");
            validatebtn.setAttribute("value", " ");
            validatebtn.className = "minesweeper-bttn smiley";
            // validate (smiley) button click
            validatebtn.onclick = function (e) {
                e.preventDefault();
                // finish game and get results
                Minesweeper.complete(Table.validate());
            };
            // cheat button
            var cheatbtn = document.createElement("input");
            cheatbtn.setAttribute("type", "button");
            cheatbtn.setAttribute("id", "minesweeper-cheat");
            cheatbtn.setAttribute("value", "Cheat");
            cheatbtn.className = "minesweeper-bttn";
            // cheat button click
            cheatbtn.onclick = function () {
                Table.revealMines();
                return false;
            };
            // append controls to the containing form
            frm.appendChild(statusdiv);
            frm.appendChild(newgamebtn);
            frm.appendChild(msTable);
            frm.appendChild(validatebtn);
            frm.appendChild(cheatbtn);


            options = options || {};
            var reg = /\D/;
            var numMines = options.mines, cols = options.columns, rows = options.rows;
            // basic validation 
            if (reg.test(cols) || reg.test(rows) || reg.test(numMines)) {
                statusdiv.innerHTML = "Enter numbers only";
                statusdiv.classList.add("alert-error");
                return;
            }
            if (numMines > cols * rows) {
                statusdiv.innerHTML = "Number of mines must be < the Board Size";
                statusdiv.classList.add("alert-error");
                return;
            }
            // create table
            Table.reset(rows, cols, numMines).create();
            statusdiv.innerHTML = "Game Started";
            document.getElementById("minesweeper").classList.remove("fail");
            document.getElementById("minesweeper").classList.add("active");
        }
    };
    // ref to Minewsweeper for external use
    window.minesweeper = Minesweeper;
})();

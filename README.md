# README #

Minesweeper

few lines of code, one javascript file that contains the entire game module, and one CSS file.

Reuasable, droppable, and easily customizable thru straight-forward CSS.


How to use

Custom settings:

* make sure HTML's minesweeper form has an id that matches the formId value in the init

* Adjust the difficulty by setting the # mines; low for easy or high (close to rows * cols) for hard

* control the board size by changing rows and columns


```
#!html

minesweeper.init({ formId: 'minesweeper-frm', mines: 10, rows: 8, columns: 8 });
```


CSS:

```
#!html

<link rel="stylesheet" href="minesweeper.css" />
```

JS:


```
#!html

 <form id="minesweeper-frm"></form>
 <script src="minesweeper.js"></script>
 <script>
// Custom settings
// make sure HTML's minesweeper form has an id that matches the formId value in the init
// Adjust the difficulty by setting the # mines; low for easy or high (close to rows * cols) 
// for hard
// control the board size by changing rows and columns
minesweeper.init({ formId: 'minesweeper-frm', mines: 10, rows: 8, columns: 8 });
</script>
```